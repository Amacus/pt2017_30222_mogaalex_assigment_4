import javax.swing.*;

/**
 * Created by Amacus5 on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Login log = new Login();
        JFrame frame = new JFrame("Login");
        frame.setContentPane( log.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
