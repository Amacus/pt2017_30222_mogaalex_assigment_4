import java.util.HashMap;
import java.util.List;

/**
 * Created by Amacus5 on 08.05.2017.
 */
public abstract class Views {
    Bank hm ;
    Person person ;
    Account account;
    HashMap<Account,List<Views>> views = new HashMap<Account,List<Views>>();
    HashMap<Account,List<String>> notification = new HashMap<>();

    public Views(Bank hm, Person person, Account account, HashMap<Account, List<Views>> views,HashMap<Account, List<String>> notification) {
        this.hm = hm;
        this.person = person;
        this.account = account;
        this.views = new HashMap<Account, List<Views>>(views);
        this.notification = new HashMap<Account, List<String>>(notification);
    }
    public abstract void updateAdmin(Bank hm);
    public abstract void notificationUpdate(HashMap<Account, List<String>> notification,Account acc, List<String> msj);
    public abstract void showNotification(HashMap<Account, List<String>> notification,Account acc);
}
