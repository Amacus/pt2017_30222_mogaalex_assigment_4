import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public class AdminView extends  Views {
    private JTable table1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JButton createButton;
    private JButton withdrawButton;
    private JButton addButton;
    private JButton editButton;
    private JButton removeButton;
    public JPanel panel1;
    private JTextField textField5;
    private JRadioButton savingAccountRadioButton;
    private JRadioButton spendingAccountRadioButton;
    private JTextField textField6;
    private static int idCounter = 1;

    private Set<Account> AccountsList(HashMap views){
        return views.keySet();
    }
    private void generateId(Bank hm){
        int max = 0;
        for (Person person: hm.ClientList()) {
            if(max < person.getId()) {
                max = person.getId();
            }
            idCounter = max+1;
        }
    }
    public static TableModel toTableModel(Bank hm) {
        DefaultTableModel model = new DefaultTableModel(
                new Object[] { "Key", "Name", "Saving", "Spending","Online" }, 0
        );
        for (Person person1: hm.ClientList()) {
            if(hm.getHm().get(person1).get(1) == null) {
                model.addRow(new Object[]{person1.getId(), person1.getNume(), hm.getHm().get(person1).get(0).getBalance(), "empty",hm.getHm().get(person1).get(0).isOn()});
            }else if(hm.getHm().get(person1).get(0) == null){
                model.addRow(new Object[]{person1.getId(), person1.getNume(), "empty", hm.getHm().get(person1).get(1).getBalance(),hm.getHm().get(person1).get(1).isOn()});
            }else{
                model.addRow(new Object[]{person1.getId(), person1.getNume(), hm.getHm().get(person1).get(0).getBalance(), hm.getHm().get(person1).get(1).getBalance(),hm.getHm().get(person1).get(0).isOn()});
            }

        }
        return model;
    }
    @Override
    public void updateAdmin(Bank hm) {

        try
        {
            FileOutputStream fos =
                    new FileOutputStream("hashmap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(hm.getHm());
            oos.close();
            fos.close();
            System.out.printf("Serialized HashMap data is saved in hashmap.ser");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
        table1.setModel(toTableModel(hm));
    }

    @Override
    public void notificationUpdate(HashMap<Account, List<String>> notification, Account acc, List<String> msj) {

    }

    @Override
    public void showNotification(HashMap<Account, List<String>> notification, Account acc) {

    }


    public AdminView(Bank hm, Person person, Account account, HashMap<Account, List<Views>> views, HashMap<Account, List<String>> notification) {
        super(hm,person,account,views,notification);
        updateAdmin(hm);
        for(Account acc1: AccountsList(views)){
            for(int i=0;i<views.get(acc1).size();i++)
                if(views.get(acc1).get(i) != null)
                    views.get(acc1).get(i).updateAdmin(hm);
        }
        JFrame frame = new JFrame("Admin");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                account.setOn(false);
                for(Account acc1: AccountsList(views)){
                    for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null)
                            views.get(acc1).get(i).updateAdmin(hm);
                }
                e.getWindow().dispose();
            }
        });
        frame.pack();
        frame.setVisible(true);
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generateId(hm);
                Person person = new Person(idCounter, textField1.getText());
                Account acc = null;
                List<Account> accounts = new ArrayList<>(2);
                accounts.add(0,null);
                accounts.add(1,null);
               hm.create(savingAccountRadioButton, spendingAccountRadioButton, acc,  accounts, person, textField3, textField4, textField5);
                for(Account acc1: AccountsList(views)){
                    for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null)
                            views.get(acc1).get(i).updateAdmin(hm);
                }

            }
        });
        withdrawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hm.withdraw(savingAccountRadioButton, spendingAccountRadioButton,textField2,textField6, notification);

                    for(Account acc1: AccountsList(views)){
                    for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null) {
                            views.get(acc1).get(i).updateAdmin(hm);
                            views.get(acc1).get(i).showNotification(notification,acc1);
                        }
                }
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hm.add(savingAccountRadioButton, spendingAccountRadioButton,textField2,textField6, notification);
                System.out.println("baniiiii : " + hm.getHm().get(person).get(0).getBalance());
                for(Account acc1: AccountsList(views)){
                    for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null) {
                            views.get(acc1).get(i).updateAdmin(hm);
                            views.get(acc1).get(i).showNotification(notification,acc1);
                        }
                }
            }
        });
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hm.remove(textField2);
                for(Account acc1: AccountsList(views)){
                    for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null)
                            views.get(acc1).get(i).updateAdmin(hm);
                }
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hm.edit(textField1,textField2, textField3,textField4,notification, spendingAccountRadioButton,savingAccountRadioButton );
                    for(Account acc1: AccountsList(views)){
                        for(int i=0;i<views.get(acc1).size();i++)
                        if(views.get(acc1).get(i) != null) {
                            views.get(acc1).get(i).updateAdmin(hm);
                            views.get(acc1).get(i).showNotification(notification,acc1);
                        }
                    }
            }
        });
    }

}
