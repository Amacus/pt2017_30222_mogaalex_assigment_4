/**
 * Created by Amacus5 on 03.05.2017.
 */
public class SpendingAccount extends Account {
    public SpendingAccount(String user, String pass, int id, double balance,boolean on) {
        super(user, pass, id, balance,on);
    }

    @Override
    public void add(int amount) {
        balance = balance + amount;
    }

    @Override
    public void withdraw(int amount) {
        if(balance >= amount)
            balance = balance - amount;
    }
}
