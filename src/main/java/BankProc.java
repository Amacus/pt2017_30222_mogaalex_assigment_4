import javax.swing.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public interface BankProc {
    void create(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton, Account acc, List<Account> accounts, Person person, JTextField textField3, JTextField textField4, JTextField textField5);
    void edit( JTextField textField1, JTextField textField2, JTextField textField3, JTextField textField4, HashMap<Account, List<String>> notification, JRadioButton spendingAccountRadioButton, JRadioButton savingAccountRadioButton);
    void remove( JTextField textField2);
    void withdraw(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton, JTextField textField2, JTextField textField6, HashMap<Account,List<String>> notification);
    void add(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton, JTextField textField2, JTextField textField6, HashMap<Account,List<String>> notification);
}
