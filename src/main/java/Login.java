import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public class Login {
    private JTextField textField1;
    private JTextField textField2;
    private JButton loginButton;
    public JPanel panel1;
    Bank bank = new Bank();
    HashMap<Account,List<Views>> views = new HashMap<>();
    HashMap<Account,List<String>> notification = new HashMap<>();
    public void newBank(){
        try
        {
            FileInputStream fis = new FileInputStream("hashmap.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            bank.setHm(null);
            bank.setHm((HashMap) ois.readObject());
            fis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
        System.out.println("Deserialized HashMap..");
        // Display content using Iterator
        for (Person person1: bank.getHm().keySet()) {
            for (int i = 0; i < bank.getHm().get(person1).size(); i++) {
                if(bank.getHm().get(person1).get(i)!=null)
                System.out.println("value : " + bank.getHm().get(person1).get(i).getUser() + " " + bank.getHm().get(person1).get(i).getPass());
            }
        }
        System.out.println("finish");
    }

    public Login() {
        Person p = new Person(3,"Alex");
        Account a = new SavingAccount("Alex","pass",1,3.0,false);
        newBank();
        List<Account> acc = new ArrayList<>(2);
        acc.add(0,a);
        acc.add(1,null);
        //bank.addClient(acc,p);
        for(Person pers:bank.ClientList()) {
            List<String> l = new ArrayList<>();
            for(int i = 0;i<bank.getHm().get(pers).size();i++)
                if(bank.getHm().get(pers).get(i) !=null)
                    notification.put(bank.getHm().get(pers).get(i),l);
        }

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkpass = 0;
                int checkuser = 0;
                int checkok = 0;
                //List<Person> persons = new ArrayList<Person>(bank.ClientList());
                for (Person person : bank.ClientList()) {
                    for (int i = 0; i < bank.getHm().get(person).size(); i++) {
                        List<Views> vs = new ArrayList<>(2);
                        vs.add(0, null);
                        vs.add(1, null);
                        if (bank.getHm().get(person).get(i) != null) {
                            System.out.println("value : " + bank.getHm().get(person).get(i).getUser());
                            if (bank.getHm().get(person).get(i).getUser().equals(textField1.getText())) {
                                if (bank.getHm().get(person).get(i).getPass().equals(textField2.getText())) {
                                    if (bank.getHm().get(person).get(i).getId() == 0) {
                                        checkpass = 1;
                                        bank.getHm().get(person).get(i).setOn(true);
                                        Views cw = new ClientView(bank, person, bank.getHm().get(person).get(i), views, notification);
                                        vs.add(0, cw);
                                        views.put(bank.getHm().get(person).get(i), vs);
                                    } else {
                                        checkpass = 1;
                                        if (checkok == 0) {
                                            bank.getHm().get(person).get(i).setOn(true);
                                            Views aw = new AdminView(bank, person, bank.getHm().get(person).get(i), views, notification);
                                            vs.add(1, aw);
                                            views.put(bank.getHm().get(person).get(i), vs);
                                            checkok = 1;
                                        }
                                    }
                                }
                                checkuser = 1;
                            }
                        }
                    }
                }
                if (checkuser == 0) {
                    JOptionPane.showMessageDialog(null, "Wrong Username");
                }
                if (checkpass == 0) {
                    JOptionPane.showMessageDialog(null, "Wrong Password");
                }
            }
        });
    }

}
