import java.io.Serializable;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public abstract class  Account implements Serializable {
    String user = new String();
    String pass = new String();
    int id;
    double balance;
    boolean on = false;

    public Account(String user, String pass, int id, double balance, boolean on) {
        this.user = user;
        this.pass = pass;
        this.id = id;
        this.balance = balance;
        this.on = on;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public abstract void add(int amount);
    public abstract void withdraw(int amount);
}
