import java.io.Serializable;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public class Person implements Serializable {
    int id;
    String nume = new String();

    public Person(int id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}
