import javax.swing.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public class Bank extends HashMap implements Serializable,BankProc {
    private HashMap<Person,List<Account>> hm = new HashMap<>();
    public void addClient( List<Account> accounts, Person pers){
        hm.put(pers,accounts);
    }
    public Set<Person> ClientList(){
        return hm.keySet();
    }
    public HashMap<Person, List<Account>> getHm() {
        return hm;
    }

    public void setHm(HashMap<Person, List<Account>> hm) {
        this.hm = hm;
    }

    @Override
    public void create(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton, Account acc, List<Account> accounts, Person person, JTextField textField3, JTextField textField4, JTextField textField5) {
        if( textField3.getText().equals("") || textField4.getText().equals("") || textField5.getText().equals(""))
           throw new IllegalArgumentException("Empty Fields ");
        if (savingAccountRadioButton.isSelected()) {
            acc = new SavingAccount(textField3.getText(), textField4.getText(), Integer.parseInt(textField5.getText()), 0.0,false);
            accounts.add(0,acc);
            this.addClient(accounts,person);
        }
        if (spendingAccountRadioButton.isSelected()) {
            acc = new SpendingAccount(textField3.getText(), textField4.getText(), Integer.parseInt(textField5.getText()), 0.0,false);
            accounts.add(1,acc);
            this.addClient(accounts,person);
        }
    }

    @Override
    public void edit( JTextField textField1, JTextField textField2, JTextField textField3, JTextField textField4, HashMap<Account, List<String>> notification, JRadioButton spendingAccountRadioButton,JRadioButton savingAccountRadioButton ) {
        if( textField3.getText().equals("") || textField4.getText().equals("") || textField1.getText().equals("")|| textField2.getText().equals(""))
            throw new IllegalArgumentException("Empty Fields ");
        for(Person pers:this.ClientList()){
            if(pers.getId() == Integer.parseInt(textField2.getText())){
                pers.setNume(textField1.getText());
                if( this.getHm().get(pers).get(0) != null ) {
                    this.getHm().get(pers).get(0).setUser(textField3.getText());
                    this.getHm().get(pers).get(0).setPass(textField4.getText());
                    notification.get(this.getHm().get(pers).get(0)).add("Admin edited your saving account");
                }
                if( this.getHm().get(pers).get(1) != null ) {
                    this.getHm().get(pers).get(1).setUser(textField3.getText());
                    this.getHm().get(pers).get(1).setPass(textField4.getText());
                    notification.get(this.getHm().get(pers).get(0)).add("Admin edited your spending account");
                }
                if( this.getHm().get(pers).get(1) == null && spendingAccountRadioButton.isSelected()){
                    this.getHm().get(pers).add(1, new SpendingAccount(this.getHm().get(pers).get(0).getUser(), this.getHm().get(pers).get(0).getPass(),this.getHm().get(pers).get(0).getId(),0.0,false));
                    notification.get(this.getHm().get(pers).get(0)).add("Admin edited your spending account");
                }
                if( this.getHm().get(pers).get(0) == null && savingAccountRadioButton.isSelected()){
                    this.getHm().get(pers).add(0, new SpendingAccount(this.getHm().get(pers).get(1).getUser(), this.getHm().get(pers).get(1).getPass(),this.getHm().get(pers).get(1).getId(),0.0,false));
                    notification.get(this.getHm().get(pers).get(0)).add("Admin edited your saving account");
                }
            }
        }
    }

    @Override
    public void remove(JTextField textField2) {
        if( textField2.getText().equals("") )
            throw new IllegalArgumentException("Empty Fields ");
        Person p=null;
        for(Person pers:this.ClientList()){
            if(pers.getId()==Integer.parseInt(textField2.getText())){
                p = pers;
            }
        }
        this.getHm().remove(p);

    }

    @Override
    public void withdraw(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton,JTextField textField2,JTextField textField6,HashMap<Account,List<String>> notification) {
        if( textField6.getText().equals("") || textField2.getText().equals(""))
            throw new IllegalArgumentException("Empty Fields ");
        for(Person pers:ClientList()) {
            if (pers.getId() == Integer.parseInt(textField2.getText())) {
                if (this.getHm().get(pers).get(1).getBalance()-Integer.parseInt(textField6.getText())<0)
                    throw new IllegalArgumentException("Insufficient funds");
                if (this.getHm().get(pers).get(0) != null && savingAccountRadioButton.isSelected()) {
                    this.getHm().get(pers).get(0).withdraw(Integer.parseInt(textField6.getText()));
                    notification.get(this.getHm().get(pers).get(0)).add("Admin has withdrawn money from your saving account");
                    assert this.getHm().get(pers).get(0).getBalance()>=0:"Insufficient funds";
                }
                if (this.getHm().get(pers).get(1) != null && spendingAccountRadioButton.isSelected()) {
                    this.getHm().get(pers).get(1).withdraw(Integer.parseInt(textField6.getText()));
                    notification.get(this.getHm().get(pers).get(1)).add("Admin has withdrawn money from your spending account");
                    assert this.getHm().get(pers).get(1).getBalance()>=0:"Insufficient funds";
                }
            }
        }
    }

    @Override
    public void add(JRadioButton savingAccountRadioButton, JRadioButton spendingAccountRadioButton, JTextField textField2, JTextField textField6, HashMap<Account, List<String>> notification) {
        for(Person pers:this.ClientList()) {
            if (pers.getId() == Integer.parseInt(textField2.getText())) {
                if (this.getHm().get(pers).get(0) != null && savingAccountRadioButton.isSelected()) {
                    this.getHm().get(pers).get(0).add(Integer.parseInt(textField6.getText()));
                    notification.get(this.getHm().get(pers).get(0)).add("Admin added money in your saving account");
                }
                if (this.getHm().get(pers).get(1) != null && spendingAccountRadioButton.isSelected()) {
                    this.getHm().get(pers).get(1).add(Integer.parseInt(textField6.getText()));
                    notification.get(this.getHm().get(pers).get(1)).add("Admin added money in your spending account");

                }
            }
        }
    }
}
