import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Amacus5 on 03.05.2017.
 */
public class ClientView extends Views {
    private JTextField textField1;
    private JButton addButton;
    private JButton withdrawButton;
    public JPanel panel1;
    private JTextField textField2;
    public Set<Account> AccountsList(HashMap views){
        return views.keySet();
    }
    @Override
    public void updateAdmin(Bank hm) {
        try {
            FileOutputStream fos =
                    new FileOutputStream("hashmap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(hm.getHm());
            oos.close();
            fos.close();
            System.out.printf("Serialized HashMap data is saved in hashmap.ser");
            AdminView.toTableModel(hm);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    public void notificationUpdate(HashMap<Account, List<String>> notification,Account acc, List<String> msj){
        notification.put(acc,msj);
    }
    public void showNotification(HashMap<Account, List<String>> notification,Account acc){
        if(notification.get(acc) != null)
            for (Iterator<String> it = notification.get(acc).iterator(); it.hasNext(); ) {//show notifications
                String s = it.next();
            JOptionPane.showMessageDialog(null, s);
                it.remove();
        }
    }

     ClientView(Bank hm, Person person, Account account, HashMap<Account, List<Views>> views, HashMap<Account, List<String>> notification) {
        super(hm,person,account, views, notification);
        JFrame frame = new JFrame("Client");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                account.setOn(false);
                    for(Account acc: AccountsList(views)){
                        for(int i=0;i<views.get(acc).size();i++)
                        if(views.get(acc).get(i) != null) {
                            views.get(acc).get(i).updateAdmin(hm);
                        }
                    }
                views.remove(account);
                e.getWindow().dispose();
            }
        });
        frame.pack();
        frame.setVisible(true);
        showNotification(notification,account);
        updateAdmin(hm);
         for(Account acc: AccountsList(views)){
             for(int i=0;i<views.get(acc).size();i++)
                 if(views.get(acc).get(i) != null)
                     views.get(acc).get(i).updateAdmin(hm);
         }
        textField2.setText(String.valueOf(account.getBalance()));
        withdrawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if( textField1.getText().equals(""))
                    throw new IllegalArgumentException("Empty Fields ");
                if( Integer.parseInt(textField1.getText())<0)
                    throw new IllegalArgumentException("Negative number ");
                if( account.getBalance() - Integer.parseInt(textField1.getText())<0)
                    throw new IllegalArgumentException("Insufficient funds ");
                account.withdraw(Integer.parseInt(textField1.getText()));
                assert account.getBalance()<0: "Insufficient funds";
                textField2.setText(String.valueOf(account.getBalance()));
                for(Account acc: AccountsList(views)){
                    for(int i=0;i<views.get(acc).size();i++)
                        if(views.get(acc).get(i) != null)
                            views.get(acc).get(i).updateAdmin(hm);
                }

            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if( textField1.getText().equals(""))
                    throw new IllegalArgumentException("Empty Fields ");
                if( Integer.parseInt(textField1.getText())<0)
                    throw new IllegalArgumentException("Negative number ");
                account.add(Integer.parseInt(textField1.getText()));
                textField2.setText(String.valueOf(account.getBalance()));
                for(Account acc: AccountsList(views)){
                    for(int i=0;i<views.get(acc).size();i++)
                        if(views.get(acc).get(i) != null)
                            views.get(acc).get(i).updateAdmin(hm);
                }
            }
        });
    }


}
